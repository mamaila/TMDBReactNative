import React from 'react'
import { StyleSheet, 
         Text, 
         View, 
         TextInput, 
         KeyboardAvoidingView, 
         Platform,
         Keyboard,
         StatusBar
       } from 'react-native'

import debounce from './lib/debounce'
import searchTMDB from './lib/searchTMDB'
import uniqueArray from './lib/uniqueArray'
import MovieList from './components/MovieList'

const INITIAL_STATE = {
        query: "",
        message: "Search for great movies from TMDB!",
        movies: [],
        page: 0,
        totalPages: 0,
        totalResults: 0
      }

export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = INITIAL_STATE
  }

  userTypes = debounce((query) => {

    this.setState({query: query.trim()})
    this.searchForMovies()
  }, 300)

  searchForMovies = () => {

    if (this.state.query == "") {
      this.setState(INITIAL_STATE)   
    } else {
      this.setState(Object.assign({}, INITIAL_STATE, { message: "Searching...", query: this.state.query }))
      this._handleTMDBResponse(searchTMDB(this.state.query))
    }
  }

  _parseResponse = (response) => {

    // sometimes tmdb returns duplicate movies in 2 consecutive pages
    const movies = uniqueArray(this.state.movies.concat(response.results))
    const message = (this.state.query !== "" && movies.length == 0) 
                      ? "No results matching your query"
                      : `Fetched page ${response.page} of ${response.total_pages}`
    
    this.setState({
          movies: movies,
          page: response.page,
          totalPages: response.total_pages,
          totalResults: response.total_results,
          message: message
        })
  }

  loadMore = debounce(() => {

    if (this.state.totalPages > this.state.page) {
      this.setState({message: "Loading more..."})
      this._handleTMDBResponse(searchTMDB(`${this.state.query}&page=${this.state.page + 1}`))
    }

    if (this.state.totalPages == this.state.page) {
      this.setState({message: "All results loaded"})
    }
  }, 300)

  _handleTMDBResponse = (response) => {

    return response
            .then(this._parseResponse)
            .catch((e) => this.setState({message: e.message}))
  }

  render() {

    return (
      <View style={styles.container}>
        <StatusBar />
        <View style={styles.topBar}>
          <TextInput
            style={styles.searchInput}
            autoFocus= {true}
            autoCorrect= {false}
            autoCapitalize= "none"
            placeholder="Type to search TMDB!"
            clearButtonMode="always"
            onChangeText={this.userTypes}
          />
          <Text style={styles.messages}>{this.state.message}</Text>
        </View>

        <KeyboardAvoidingView style={styles.movieList} behavior="padding" enabled>
          <MovieList data={ this.state.movies }
                     loadMore={ this.loadMore }
          />
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column'
  },
  topBar: {
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    height: 110,
    paddingTop: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight 
  },
  searchInput: {
    height: 40,
    width: 300,
    backgroundColor: '#fff',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    padding: 5
  },
  movieList: {
    flex: 1,
    backgroundColor: 'skyblue'
  },
  messages: {
    padding: 10,
    fontSize: 18,
    height: 44,
    color: 'red'
  }
})
