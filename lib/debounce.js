// ./lib/debounce.js

// debounce :: (Function, Int) -> Function
export default function debounce(fn, delay) {
  
  let timerId

  return function (...args) {
    
    clearTimeout(timerId)

    timerId = setTimeout(() => {
      fn(...args)
      timerId = null
    }, delay)
  }
}