// ./lib/uniqueArray.js

// uniqueArray :: Array -> Array
export default uniqueArray = a => [...new Set(a.map(o => JSON.stringify(o)))].map(s => JSON.parse(s))