// ./lib/searchTMDB.js

import memoize from './memoize'

const API_KEY = "6b8ee4af2b878b78f719db3bc4ef6053"
const API_URL = "https://api.themoviedb.org/3"
const SEARCH_URL = `${API_URL}/search/movie?api_key=${API_KEY}`


const checkResponse = (response) => {

  if (response.ok && !response.bodyUsed) {
    return response.json()
  }

  throw new Error("Error from TMDB")
}

// searchTMDB :: String -> Promise
function search(query) {

  const searchQuery = `${SEARCH_URL}&query=${query}`

  console.log("SEARCH PATH IS", searchQuery)

  return fetch(searchQuery).then(checkResponse)
}

export default searchTMDB = memoize(search)