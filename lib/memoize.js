// ./lib/memoize.js

// memoize :: fn -> fn
export default function memoize(fn, keyMaker = JSON.stringify) {
  const lookupTable = {}

  return function (...args) {
    const key = keyMaker.apply(this, args)

    return lookupTable[key] || (lookupTable[key] = fn.apply(this, args))
  }
}