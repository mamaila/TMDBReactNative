// ./components/MovieList.js

import React from 'react'
import { StyleSheet, Text, View, TextInput, FlatList } from 'react-native'

export default class MovieList extends React.Component {

  constructor(props) {
    super(props)
  }

  _keyExtractor = (item, index) => `${item.id}`

  render() {
    return (
      <FlatList
        style={styles.flatList}
        keyboardDismissMode="on-drag"
        keyExtractor={this._keyExtractor}
        data={this.props.data}
        onEndReached={this.props.loadMore}
        renderItem={({item}) => <Text style={styles.item}>{item.title}</Text>}
      />
    )
  }
}

const styles = StyleSheet.create({
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  flatList: {
    flex: 1,
  }
})